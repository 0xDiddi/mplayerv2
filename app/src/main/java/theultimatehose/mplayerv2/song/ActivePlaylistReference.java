package theultimatehose.mplayerv2.song;

import java.util.ArrayList;
import java.util.List;

public class ActivePlaylistReference {

    private String ref;
    private int index;

    public static ActivePlaylistReference ALL_SONGS_REF = new ActivePlaylistReference("all");
    public static ActivePlaylistReference FAVOURITES_REF = new ActivePlaylistReference("fav");

    private ActivePlaylistReference(String ref) {
        this.ref = ref;
    }

    private ActivePlaylistReference(String ref, int index) {
        this.ref = ref;
        this.index = index;
    }

    public static ActivePlaylistReference createReferenceForArtist(int artistIndex) {
        return new ActivePlaylistReference("artist", artistIndex);
    }

    public static ActivePlaylistReference createReferenceForAlbum(int albumIndex) {
        return new ActivePlaylistReference("album", albumIndex);
    }

    public static ActivePlaylistReference createReferenceForPlaylist(int listIndex) {
        return new ActivePlaylistReference("list", listIndex);
    }

    public List<Integer> dereference() {
        switch (ref) {
            case "all":
                return SongManager.getIntList();
            case "artist":
                return AdapterUtil.getSongsForArtist(AdapterUtil.getArtists().get(index));
            case "album":
                return AdapterUtil.getSongsForAlbum(AdapterUtil.getAlbums().get(index));
            case "list":
                return PlaylistManager.playlists.get(index).songs;
            case "fav":
                return PlaylistManager.favourites.songs;
        }
        return new ArrayList<>();
    }

}
