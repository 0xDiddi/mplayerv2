package theultimatehose.mplayerv2.song;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.adapter.songlist.SongQueueAdapter;
import theultimatehose.mplayerv2.util.SeekBarChangeListenerWrapper;
import theultimatehose.mplayerv2.util.UnitConverter;

public class Player {

    public static MediaPlayer player = new MediaPlayer();
    public static Song currentSong;

    private static Activity context;

    static {
        player.setOnPreparedListener(mp -> play());

        player.setOnCompletionListener(mp -> playNext());
    }

    @SuppressLint("SetTextI18n")
    public static void setupUiUpdater(Activity context) {
        Player.context = context;
        new Thread(() -> {
            int lastDuration = 0;
            while (true) {
                int sleepTime = isPlaying() ? 100 : 1000;

                if (currentSong != null) {
                    int duration = isPlaying() ? player.getDuration() : lastDuration;
                    if (isPlaying())
                        lastDuration = player.getDuration();

                    context.runOnUiThread(() -> {
                        SeekBar bar = context.findViewById(R.id.fragment_seekbar);
                        bar.setMax(duration);
                        if (!bar.isPressed())
                            bar.setProgress(player.getCurrentPosition());
                        bar.setOnSeekBarChangeListener(new SeekBarChangeListenerWrapper() {
                            @Override
                            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                                if (fromUser) player.seekTo(progress);
                            }
                        });

                        ((TextView) context.findViewById(R.id.fragment_txt_title)).setText(currentSong.title);
                        ((TextView) context.findViewById(R.id.fragment_txt_artist)).setText(currentSong.artist);

                        ((TextView) context.findViewById(R.id.fragment_txt_time_current)).setText(UnitConverter.msToTimeString(player.getCurrentPosition()));
                        ((TextView) context.findViewById(R.id.fragment_txt_time_remaining)).setText("-" + UnitConverter.msToTimeString(duration - player.getCurrentPosition()));
                        ((TextView) context.findViewById(R.id.fragment_txt_time_total)).setText(UnitConverter.msToTimeString(duration));

                        ImageButton playButton = context.findViewById(R.id.fragment_btn_play);
                        playButton.setImageResource(isPlaying() ? R.drawable.ic_pause : R.drawable.ic_play);
                        playButton.setOnClickListener(v -> toggle());

                        ImageButton modeButton = context.findViewById(R.id.fragment_btn_mode);
                        modeButton.setImageResource(Queue.currentPlayMode.getDrawable());
                        modeButton.setOnClickListener(v -> Queue.currentPlayMode = Queue.currentPlayMode.next());

                        ImageButton favButton = context.findViewById(R.id.fragment_btn_fav);
                        boolean isFav = PlaylistManager.favourites.songs.contains(SongManager.songList.indexOf(currentSong));
                        favButton.setImageResource(isFav ? R.drawable.ic_favourite : R.drawable.ic_favourite_border);
                        favButton.setOnClickListener(v -> {
                            int songIndex = SongManager.songList.indexOf(currentSong);
                            boolean localIsFav = PlaylistManager.favourites.songs.contains(songIndex);
                            if (localIsFav) {
                                PlaylistManager.favourites.songs.remove(songIndex);
                                Snackbar.make(PlayerActivity.instance.findViewById(R.id.activity_coordinator), PlayerActivity.instance.getString(R.string.snackbar_remove_from_favourites, currentSong.title), Snackbar.LENGTH_SHORT).show();

                            } else {
                                PlaylistManager.favourites.songs.add(songIndex);
                                Snackbar.make(PlayerActivity.instance.findViewById(R.id.activity_coordinator), PlayerActivity.instance.getString(R.string.snackbar_add_to_favourites, currentSong.title), Snackbar.LENGTH_SHORT).show();
                            }
                            RecyclerView rv = context.findViewById(R.id.frame_list_recycler);
                            if (rv != null) {
                                rv.getAdapter().notifyDataSetChanged();
                            }
                        });


                        context.findViewById(R.id.fragment_btn_next).setOnClickListener(v -> playNext());

                        ImageView img = context.findViewById(R.id.frame_player_image);
                        if (img != null) img.setImageDrawable(currentSong.getCoverImage());
                    });
                }

                try {
                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void startService() {
        Intent intent = new Intent(context.getApplicationContext(), PlayerService.class);
        intent.setAction(PlayerService.ACTION_REFRESH);
        context.startService(intent);
    }

    public static boolean isPlaying() {
        return player.isPlaying();
    }

    public static void play() {
        player.start();
        refreshService();
    }

    public static void playNext() {
        currentSong = Queue.next();

        RecyclerView v = context.findViewById(R.id.frame_list_recycler);
        RecyclerView.Adapter a;
        if (v != null && (a = v.getAdapter()) instanceof SongQueueAdapter) {
            a.notifyDataSetChanged();
            ((LinearLayoutManager) v.getLayoutManager()).scrollToPositionWithOffset(Queue.history.size(), 0);
        }

        if (currentSong == null) return;
        try {
            player.reset();
            player.setDataSource(currentSong.path);
            player.prepareAsync();
        } catch (Exception e) {
            Log.e("MPlayer v2", "An error occurred while preparing the media player.", e);
        }
        refreshService();
    }

    public static void pause() {
        player.pause();
        refreshService();
    }

    public static void stop() {
        player.stop();
        refreshService();
    }

    public static void toggle() {
        if (isPlaying()) pause();
        else play();
    }

    private static void refreshService() {
        Intent intent = new Intent(context.getApplicationContext(), PlayerService.class);
        intent.setAction(PlayerService.ACTION_REFRESH);
        context.startService(intent);
    }

}
