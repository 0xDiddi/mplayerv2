package theultimatehose.mplayerv2.song;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.IBinder;

import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.util.UnitConverter;

public class PlayerService extends Service {

    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_PAUSE = "action_pause";
    public static final String ACTION_NEXT = "action_next";
    public static final String ACTION_REFRESH = "action_refresh";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void handleIntent(Intent i) {
        if (i == null || i.getAction() == null) return;

        String action = i.getAction();

        if (action.equalsIgnoreCase(ACTION_PLAY))
            Player.play();
        else if (action.equalsIgnoreCase(ACTION_PAUSE))
            Player.pause();
        else if (action.equalsIgnoreCase(ACTION_NEXT))
            Player.playNext();
        else if (action.equalsIgnoreCase(ACTION_REFRESH))
            buildNotification();

    }

    private Notification.Action generateAction(int icon, String title, String intentAction) {
        Intent intent = new Intent(getApplicationContext(), PlayerService.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new Notification.Action.Builder(icon, title, pendingIntent).build();
    }

    private void buildNotification() {
        Notification.MediaStyle style = new Notification.MediaStyle();

        Intent intent = new Intent(getApplicationContext(), PlayerService.class);
        intent.setAction(ACTION_PAUSE);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        Notification.Builder builder = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_play)
                //.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(Player.currentSong != null ? Player.currentSong.title : getString(R.string.fault_no_song))
                .setContentText(Player.currentSong != null ? (Player.currentSong.artist + (Player.isPlaying() ? " (" + UnitConverter.msToTimeString(Player.player.getDuration()) + ")" : "")) : "")
                .setDeleteIntent(pendingIntent)
                .setStyle(style)
                .addAction(Player.isPlaying() ? generateAction(R.drawable.ic_pause, getString(R.string.notification_pause), ACTION_PAUSE) : generateAction(R.drawable.ic_play, getString(R.string.notification_play), ACTION_PLAY))
                .addAction(generateAction(R.drawable.ic_next, getString(R.string.notification_next), ACTION_NEXT))
                .setOngoing(true);
        style.setShowActionsInCompactView(0, 1);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(1, builder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handleIntent(intent);
        return super.onStartCommand(intent, flags, startId);
    }
}
