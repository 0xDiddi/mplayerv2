package theultimatehose.mplayerv2.song;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class PlaylistManager {

    public static List<Playlist> playlists = new ArrayList<>();
    public static Playlist favourites = new Playlist("Favourites");

    public static void addPlaylist(String name, Integer... songs) {
        playlists.add(new Playlist(name, songs));
        savePlaylists();
    }

    public static void savePlaylists() {
        File listCache = new File(PlayerActivity.instance.getFilesDir(), "playlists.json");
        listCache.mkdirs();
        listCache.delete();
        File favCache = new File(PlayerActivity.instance.getFilesDir(), "favourites.json");
        favCache.mkdirs();
        favCache.delete();

        try {
            JSONArray arr = new JSONArray();
            for (Playlist p : playlists) {
                JSONObject obj = new JSONObject();
                obj.put("name", p.name);

                JSONArray songs = new JSONArray();
                for (int i : p.songs) songs.put(i); // todo: use file paths instead to not explode when the song list changes

                obj.put("songs", songs);

                arr.put(obj);
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(listCache));
            bw.write(arr.toString());
            bw.flush();
            bw.close();
        } catch (Exception e) {
            Log.e("MPlayer v2", "Error saving playlists.", e);
        }

        try {
            JSONArray songs = new JSONArray();
            for (int i : favourites.songs) songs.put(i);

            BufferedWriter bw = new BufferedWriter(new FileWriter(favCache));
            bw.write(songs.toString());
            bw.flush();
            bw.close();
        } catch (Exception e) {
            Log.e("MPlayer v2", "Error saving favourites.", e);
        }
    }

    public static void loadPlaylists() {
        File cache = new File(PlayerActivity.instance.getFilesDir(), "playlists.json");
        if (cache.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(cache));
                JSONArray arr = new JSONArray(br.readLine());
                br.close();

                for (int i = 0; i < arr.length(); i++) {
                    JSONObject obj = arr.getJSONObject(i);
                    Playlist p = new Playlist(obj.getString("name"));
                    JSONArray songs = obj.getJSONArray("songs");
                    for (int s = 0; s < songs.length(); s++) p.songs.add(songs.getInt(s));

                    playlists.add(p);
                }
            } catch (Exception e) {
                Log.e("MPlayer v2", "Error loading playlists.", e);
            }
        }

        cache = new File(PlayerActivity.instance.getFilesDir(), "favourites.json");
        if (cache.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(cache));
                JSONArray songs = new JSONArray(br.readLine());
                br.close();

                for (int s = 0; s < songs.length(); s++) favourites.songs.add(songs.getInt(s));

            } catch (Exception e) {
                Log.e("MPlayer v2", "Error loading favourites.", e);
            }
        }

    }

    public static boolean showAddToPlaylistDialog(Song s) {
        AlertDialog.Builder ad = new AlertDialog.Builder(PlayerActivity.instance);
        ad.setTitle(R.string.dialog_select_playlist);
        ad.setItems(toCharSequences(), (dialog, which) -> {
            Playlist p = playlists.get(which);
            p.songs.add(SongManager.songList.indexOf(s));
            savePlaylists();
            Snackbar.make(PlayerActivity.instance.findViewById(R.id.activity_coordinator), PlayerActivity.instance.getString(R.string.snackbar_added_to_playlist, s.title, p.name), Snackbar.LENGTH_SHORT).show();
        });
        ad.setNegativeButton(R.string.dialog_option_cancel, null);

        ad.create().show();
        return false;
    }

    private static CharSequence[] toCharSequences() {
        CharSequence cs[] = new CharSequence[playlists.size()];
        for (int i = 0; i < playlists.size(); i++) cs[i] = playlists.get(i).name;
        return cs;
    }

    public static class Playlist implements Comparable<Playlist> {
        public String name;
        public List<Integer> songs;

        public Playlist(String name, Integer... songs) {
            this.name = name;
            this.songs = new ArrayList<>(Arrays.asList(songs));
        }

        public String getName() { // using this because the comparator can't take fields
            return name;
        }

        @Override
        public int compareTo(@NonNull Playlist o) {
            return name.compareTo(o.name);
        }
    }

}
