package theultimatehose.mplayerv2.song;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.util.Log;

import com.mpatric.mp3agic.ID3v1;
import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.Mp3File;

import java.io.ByteArrayInputStream;
import java.io.File;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;

public class Song implements Comparable<Song> {

    public String path;
    public Mp3File data;

    public String title, album, artist, genre;

    public Song(String path, boolean scan) {
        this.path = path;
        try {
            this.data = new Mp3File(path, scan);
            if (this.data.hasId3v2Tag()) {
                ID3v2 tag = this.data.getId3v2Tag();
                readTag(tag);
            } else if (this.data.hasId3v1Tag()) {
                readTag(this.data.getId3v1Tag());
            }
        } catch (Exception e) {
            Log.e("MPlayer v2", "Error reading song (" + path + "):", e);
        }
        fillFallback();
    }

    private void fillFallback() {
        if (title == null || title.isEmpty())
            title = path.substring(path.lastIndexOf(File.separator) + 1);
        if (album == null || album.isEmpty())
            album = path.substring(path.lastIndexOf(File.separator, path.lastIndexOf(File.separator) - 1) + 1, path.lastIndexOf(File.separator));
        if (artist == null || artist.isEmpty())
            artist = PlayerActivity.instance.getString(R.string.song_field_fallback);
        if (genre == null || genre.isEmpty())
            genre = PlayerActivity.instance.getString(R.string.song_field_fallback);
    }

    public boolean hasCoverImage() {
        return this.data.hasId3v2Tag() && this.data.getId3v2Tag().getAlbumImage() != null;
    }

    public Drawable getCoverImage() {
        if (this.hasCoverImage())
            return Drawable.createFromStream(new ByteArrayInputStream(this.data.getId3v2Tag().getAlbumImage()), "");
        else return null;
    }

    public String getName() {
        return title;
    }

    private void readTag(ID3v1 tag) {
        this.title = tag.getTitle();
        this.album = tag.getAlbum();
        this.artist = tag.getArtist();
        this.genre = tag.getGenreDescription();
    }

    @Override
    public int compareTo(@NonNull Song o) {
        return title.compareTo(o.title);
    }
}
