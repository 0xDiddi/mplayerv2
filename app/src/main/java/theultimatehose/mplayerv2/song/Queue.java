package theultimatehose.mplayerv2.song;

import android.support.annotation.DrawableRes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.util.Sorter;

public class Queue {

    public static List<Integer> history = new ArrayList<>();
    public static List<Integer> queue = new ArrayList<>();
    private static List<Integer> activePlaylistForecast = new ArrayList<>();

    private static ActivePlaylistReference lastActivePlaylist;
    private static ActivePlaylistReference activePlaylist;

    public static PlayMode currentPlayMode = PlayMode.Shuffle;

    private static final int activePlaylistForecastLength = 15;
    private static final int historyLength = 5;

    private static Random rng = new Random();

    public static void push(int songIndex) {
        queue.add(songIndex);
        if (!Player.isPlaying()) Player.playNext();
    }

    public static void setActivePlaylist(ActivePlaylistReference activePlaylist, boolean force) {
        if (force || Queue.activePlaylist == null)
            Queue.activePlaylist = activePlaylist;
        populatePlaylistForecast();
    }

    public static Song next() {
        Song s = nextInternal();
        populatePlaylistForecast();
        return s;
    }

    private static Song nextInternal() {
        if (currentPlayMode != PlayMode.RepeatOne) {
            if (Player.currentSong != null) {
                if (queue.size() > 0) {
                    history.add(queue.get(0));
                    queue.remove(0);
                } else if (activePlaylistForecast.size() > 0) {
                    history.add(activePlaylistForecast.get(0));
                    activePlaylistForecast.remove(0);
                }
                if (history.size() > historyLength) history.remove(0);
            }
            if (queue.size() > 0) return SongManager.songList.get(queue.get(0));
            else if (activePlaylistForecast.size() > 0) {
                queue.add(0, activePlaylistForecast.get(0));
                activePlaylistForecast.remove(0);
                return SongManager.songList.get(queue.get(0));
            }
            return null;
        } else {
            return Player.currentSong;
        }
    }

    private static Map<Integer, Integer> h = new LinkedHashMap<>();
    private static int i = 1;
    private static boolean firstPass;
    private static void populatePlaylistForecast() {
        if (activePlaylist != lastActivePlaylist) {
            activePlaylistForecast.clear();
            //firstPass = true; // todo: keep this in mind in case the gaussian algorithm fucks up
            for (int s : activePlaylist.dereference()) h.put(s,i);
        }

        if (currentPlayMode == PlayMode.RepeatOne) return;

        while (activePlaylistForecast.size() < activePlaylistForecastLength)
            activePlaylistForecast.add(populateInternal(activePlaylist.dereference()));

        lastActivePlaylist = activePlaylist;
        firstPass = false;
    }

    private static int populateInternal(List<Integer> playlist) {
        if (currentPlayMode == PlayMode.RepeatList) {
            int last;
            if (activePlaylistForecast.size() > 0)
                last = activePlaylistForecast.get(activePlaylistForecast.size() - 1);
            else last = SongManager.songList.indexOf(Player.currentSong);

            if (last >= playlist.size()) last -= playlist.size();
            return playlist.get(last + 1);
        } else if (currentPlayMode == PlayMode.Shuffle) {
            if (!firstPass) {
                h = Sorter.sortByValue(h);
                int n = (int) Math.max(0, Math.min(playlist.size() - 1, Math.abs(rng.nextGaussian()) * (playlist.size() / 4)));

                h.put((int) h.keySet().toArray()[n], i);
                i++;
                return (int) h.keySet().toArray()[n];
            }

            int next = playlist.get(rng.nextInt(playlist.size()));
            if (activePlaylistForecast.size() > 0 && playlist.size() > 1)
                while ((next = playlist.get(rng.nextInt(playlist.size()))) == activePlaylistForecast.get(activePlaylistForecast.size() - 1)) {}
            h.put(next, i);
            i++;
            return next;
        } else return -1;
    }

    public static int getAbsoluteLength() {
        return history.size() + queue.size() + activePlaylistForecast.size();
    }

    public static Song getSongFromAbsoluteIndex(int absoluteIndex) {
        if (absoluteIndex < history.size()) return SongManager.songList.get(history.get(absoluteIndex));
        if (absoluteIndex < history.size() + queue.size()) return SongManager.songList.get(queue.get(absoluteIndex - history.size()));
        if (absoluteIndex < getAbsoluteLength()) return SongManager.songList.get(activePlaylistForecast.get(absoluteIndex - history.size() - queue.size()));
        return null;
    }

    public enum PlayMode {
        RepeatList,
        RepeatOne,
        Shuffle;

        public PlayMode next() {
            switch (this) {
                case RepeatList: return RepeatOne;
                case RepeatOne: return Shuffle;
                case Shuffle: return RepeatList;
                default: return Shuffle;
            }
        }

        public @DrawableRes int getDrawable() {
            switch (this) {
                case RepeatList: return R.drawable.ic_repeat;
                case RepeatOne: return R.drawable.ic_repeat_one;
                case Shuffle: return R.drawable.ic_shuffle;
                default: return 0;
            }
        }
    }

}
