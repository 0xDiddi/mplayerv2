package theultimatehose.mplayerv2.song;

import java.util.ArrayList;
import java.util.List;

public class AdapterUtil {

    public static List<String> getAlbums() {
        ArrayList<String> albums = new ArrayList<>();
        for (Song s : SongManager.songList) if (!albums.contains(s.album)) albums.add(s.album);
        return albums;
    }

    public static List<Integer> getSongsForAlbum(String album) {
        ArrayList<Integer> songs = new ArrayList<>();
        for (int i = 0; i < SongManager.songList.size(); i++) if (SongManager.songList.get(i).album.equals(album)) songs.add(i);
        return songs;
    }

    public static List<String> getArtists() {
        ArrayList<String> artists = new ArrayList<>();
        for (Song s : SongManager.songList) if (!artists.contains(s.artist)) artists.add(s.artist);
        return artists;
    }

    public static List<Integer> getSongsForArtist(String artist) {
        ArrayList<Integer> songs = new ArrayList<>();
        for (int i = 0; i < SongManager.songList.size(); i++) if (SongManager.songList.get(i).artist.equals(artist)) songs.add(i);
        return songs;
    }

}
