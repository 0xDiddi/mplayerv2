package theultimatehose.mplayerv2.adapter.songlist.general;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.adapter.songlist.SongViewHolder;
import theultimatehose.mplayerv2.song.AdapterUtil;
import theultimatehose.mplayerv2.song.Song;
import theultimatehose.mplayerv2.song.SongManager;

public class AlbumAdapter extends RecyclerView.Adapter<SongViewHolder> {

    private List<String> albumCache;
    private RecyclerView parentView;

    public AlbumAdapter() {
        this.albumCache = AdapterUtil.getAlbums();
        Collections.sort(albumCache);
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(PlayerActivity.instance).inflate(R.layout.frame_list_item_song, parent, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        holder.txtTitle.setText(albumCache.get(position));
        int size = AdapterUtil.getSongsForAlbum(albumCache.get(position)).size();
        holder.txtArtist.setText(String.format(PlayerActivity.instance.getResources().getQuantityText(R.plurals.word_song, size).toString(), size));

        Song s = SongManager.songList.get(AdapterUtil.getSongsForAlbum(albumCache.get(position)).get(0));
        if (s.hasCoverImage()) holder.setAsyncImageFromSong(s);
        else holder.setGenericImageDisplay();

        holder.itemView.setOnClickListener(v -> {
            parentView.setAdapter(new AllSongsSubAdapter(albumCache.get(position), AllSongsSubAdapter.MODE_ALBUM));
            PlayerActivity.instance.backActionStack.push(() -> parentView.setAdapter(this));
        });
    }

    @Override
    public void onViewRecycled(SongViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.imgAlbum.getDrawable() instanceof BitmapDrawable)
            ((BitmapDrawable) holder.imgAlbum.getDrawable()).getBitmap().recycle();
        holder.setGenericImageDisplay();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        parentView = recyclerView;
    }

    @Override
    public int getItemCount() {
        return albumCache.size();
    }

}
