package theultimatehose.mplayerv2.adapter.songlist;

import android.app.Activity;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.listener.SongViewHolderClickListener;
import theultimatehose.mplayerv2.song.ActivePlaylistReference;
import theultimatehose.mplayerv2.song.PlaylistManager;
import theultimatehose.mplayerv2.song.Song;
import theultimatehose.mplayerv2.song.SongManager;

public class PlaylistAdapter extends RecyclerView.Adapter<SongViewHolder> {

    private Activity context;
    private static PlaylistManager.Playlist cachedList;
    private ActivePlaylistReference reference;


    public PlaylistAdapter(Activity context, int listIndex) {
        this.context = context;
        cachedList = PlaylistManager.playlists.get(listIndex);
        reference = ActivePlaylistReference.createReferenceForPlaylist(listIndex);
    }

    public PlaylistAdapter(Activity context) {
        this.context = context;
        cachedList = PlaylistManager.favourites;
        reference = ActivePlaylistReference.FAVOURITES_REF;
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.frame_list_item_song, parent, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        int songIndex = cachedList.songs.get(position);

        Song s = SongManager.songList.get(songIndex);
        holder.bind(s);
        holder.parentListReference = reference;
        holder.itemView.setOnClickListener(new SongViewHolderClickListener(context, songIndex, holder, s.title));
    }

    @Override
    public void onViewRecycled(SongViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.imgAlbum.getDrawable() instanceof BitmapDrawable)
            ((BitmapDrawable) holder.imgAlbum.getDrawable()).getBitmap().recycle();
        holder.setGenericImageDisplay();
    }

    @Override
    public int getItemCount() {
        return cachedList.songs.size();
    }

}
