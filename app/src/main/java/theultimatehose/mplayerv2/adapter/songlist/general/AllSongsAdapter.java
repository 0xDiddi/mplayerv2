package theultimatehose.mplayerv2.adapter.songlist.general;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.adapter.songlist.SongViewHolder;
import theultimatehose.mplayerv2.listener.SongViewHolderClickListener;
import theultimatehose.mplayerv2.song.ActivePlaylistReference;
import theultimatehose.mplayerv2.song.Queue;
import theultimatehose.mplayerv2.song.Song;
import theultimatehose.mplayerv2.song.SongManager;

public class AllSongsAdapter extends RecyclerView.Adapter<SongViewHolder> {

    private static List<Song> listCache;

    public AllSongsAdapter() {
        listCache = SongManager.songList;
        Collections.sort(listCache);
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(PlayerActivity.instance).inflate(R.layout.frame_list_item_song, parent, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        Song s = listCache.get(position);
        holder.bind(s);
        holder.parentListReference = ActivePlaylistReference.ALL_SONGS_REF;
        holder.itemView.setOnClickListener(new SongViewHolderClickListener(PlayerActivity.instance, position, holder, s.title));
    }

    @Override
    public void onViewRecycled(SongViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.imgAlbum.getDrawable() instanceof BitmapDrawable)
            ((BitmapDrawable) holder.imgAlbum.getDrawable()).getBitmap().recycle();
        holder.setGenericImageDisplay();
    }

    @Override
    public int getItemCount() {
        return listCache.size();
    }

}
