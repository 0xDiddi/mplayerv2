package theultimatehose.mplayerv2.adapter.songlist.general;

import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.adapter.songlist.SongViewHolder;
import theultimatehose.mplayerv2.listener.SongViewHolderClickListener;
import theultimatehose.mplayerv2.song.ActivePlaylistReference;
import theultimatehose.mplayerv2.song.AdapterUtil;
import theultimatehose.mplayerv2.song.Queue;
import theultimatehose.mplayerv2.song.Song;
import theultimatehose.mplayerv2.song.SongManager;

public class AllSongsSubAdapter extends RecyclerView.Adapter<SongViewHolder> {

    public static final int MODE_ALBUM = 0;
    public static final int MODE_ARTIST = 1;

    private List<Integer> songCache;
    private final int mode;
    private final String name;

    public AllSongsSubAdapter(String name, int mode) {
        this.mode = mode;
        this.name = name;
        if (mode == MODE_ALBUM)
            songCache = AdapterUtil.getSongsForAlbum(name);
        else if (mode == MODE_ARTIST)
            songCache = AdapterUtil.getSongsForArtist(name);
        Collections.sort(songCache);
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(PlayerActivity.instance).inflate(R.layout.frame_list_item_song, parent, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        Song s = SongManager.songList.get(songCache.get(position));
        holder.bind(s);
        if (mode == MODE_ARTIST)
            holder.parentListReference = ActivePlaylistReference.createReferenceForArtist(AdapterUtil.getArtists().indexOf(name));
        if (mode == MODE_ALBUM)
            holder.parentListReference = ActivePlaylistReference.createReferenceForAlbum(AdapterUtil.getAlbums().indexOf(name));

        holder.itemView.setOnClickListener(new SongViewHolderClickListener(PlayerActivity.instance, songCache.get(position), holder, s.title));
    }

    @Override
    public void onViewRecycled(SongViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.imgAlbum.getDrawable() instanceof BitmapDrawable)
            ((BitmapDrawable) holder.imgAlbum.getDrawable()).getBitmap().recycle();
        holder.setGenericImageDisplay();
    }

    @Override
    public int getItemCount() {
        return songCache.size();
    }

}
