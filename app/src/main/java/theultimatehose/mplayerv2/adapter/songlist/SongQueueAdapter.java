package theultimatehose.mplayerv2.adapter.songlist;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.song.Queue;
import theultimatehose.mplayerv2.song.Song;
import theultimatehose.mplayerv2.util.UnitConverter;

public class SongQueueAdapter extends RecyclerView.Adapter<SongViewHolder> {

    private Context context;

    public SongQueueAdapter(Context context) {
        this.context = context;
    }

    @Override
    public SongViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(this.context).inflate(R.layout.frame_list_item_song, parent, false);
        return new SongViewHolder(v);
    }

    @Override
    public void onBindViewHolder(SongViewHolder holder, int position) {
        Song s = Queue.getSongFromAbsoluteIndex(position);
        holder.bind(s);
        float elevation = 0;

        if (position < Queue.history.size())
            holder.itemView.setBackgroundResource(R.color.Grey_400);
        else if (position == Queue.history.size()) {
            holder.itemView.setBackgroundResource(R.color.Amber_500);
            elevation = 10;
        } else if (position > Queue.history.size() && (position - Queue.history.size()) < Queue.queue.size()) {
            holder.itemView.setBackgroundResource(R.color.Amber_200);
            elevation = 5;
        } else
            holder.itemView.setBackgroundResource(R.color.White);

        holder.itemView.setElevation(UnitConverter.convertDpToPixel(elevation));
    }

    @Override
    public void onViewRecycled(SongViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.imgAlbum.getDrawable() instanceof BitmapDrawable)
            ((BitmapDrawable) holder.imgAlbum.getDrawable()).getBitmap().recycle();
        holder.setGenericImageDisplay();
    }

    @Override
    public int getItemCount() {
        return Queue.getAbsoluteLength();
    }
}
