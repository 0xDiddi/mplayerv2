package theultimatehose.mplayerv2.adapter.songlist;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import theultimatehose.mplayerv2.PlayerActivity;
import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.song.ActivePlaylistReference;
import theultimatehose.mplayerv2.song.Player;
import theultimatehose.mplayerv2.song.PlaylistManager;
import theultimatehose.mplayerv2.song.Queue;
import theultimatehose.mplayerv2.song.Song;
import theultimatehose.mplayerv2.song.SongManager;
import theultimatehose.mplayerv2.util.UnitConverter;


public class SongViewHolder extends RecyclerView.ViewHolder {

    public ImageView imgAlbum;
    public TextView txtTitle, txtArtist;

    public ActivePlaylistReference parentListReference;

    public SongViewHolder(View itemView) {
        super(itemView);
        imgAlbum = itemView.findViewById(R.id.img_song_item_album);
        txtTitle = itemView.findViewById(R.id.txt_song_item_title);
        txtArtist = itemView.findViewById(R.id.txt_song_item_artist);
    }

    public void bind(Song s) {
        if (s.hasCoverImage()) setAsyncImageFromSong(s);
        else setGenericImageDisplay();

        txtTitle.setText(s.title);
        txtArtist.setText(s.artist);

        itemView.setOnCreateContextMenuListener((menu, v, menuInfo) -> {
            PlayerActivity.instance.getMenuInflater().inflate(R.menu.menu_song_context, menu);

            final int songIndex = SongManager.songList.indexOf(s);
            boolean isFav = PlaylistManager.favourites.songs.contains(songIndex);
            MenuItem favItem = menu.findItem(R.id.menu_favourite);
            if (isFav) favItem.setTitle(R.string.menu_remove_from_favourites);
            favItem.setOnMenuItemClickListener(item -> {
                boolean localIsFav = PlaylistManager.favourites.songs.contains(songIndex);
                if (localIsFav) {
                    PlaylistManager.favourites.songs.remove(PlaylistManager.favourites.songs.indexOf(songIndex));
                    Snackbar.make(PlayerActivity.instance.findViewById(R.id.activity_coordinator), PlayerActivity.instance.getString(R.string.snackbar_remove_from_favourites, s.title), Snackbar.LENGTH_SHORT).show();
                } else {
                    PlaylistManager.favourites.songs.add(songIndex);
                    Snackbar.make(PlayerActivity.instance.findViewById(R.id.activity_coordinator), PlayerActivity.instance.getString(R.string.snackbar_add_to_favourites, s.title), Snackbar.LENGTH_SHORT).show();
                }
                RecyclerView rv = PlayerActivity.instance.findViewById(R.id.frame_list_recycler);
                if (rv != null) {
                    rv.getAdapter().notifyDataSetChanged();
                }
                return true;
            });
            menu.findItem(R.id.menu_add_to_playlist).setOnMenuItemClickListener(item -> PlaylistManager.showAddToPlaylistDialog(s));
            menu.findItem(R.id.menu_play_now).setOnMenuItemClickListener(item -> {
                Queue.queue.add(1, parentListReference.dereference().get(this.getLayoutPosition()));
                Queue.setActivePlaylist(parentListReference, true);
                Player.playNext();
                return true;
            });
        });
    }

    public void disableImageDisplay() {
        ((View) imgAlbum.getParent()).setVisibility(View.GONE);
    }

    public void setGenericImageDisplay() {
        imgAlbum.setImageResource(R.drawable.ic_person_grey);
        ((View) imgAlbum.getParent()).setVisibility(View.VISIBLE);
    }

    public void setAsyncImageFromSong(final Song song) {
        new Thread(() -> {
            final Drawable d = song.getCoverImage();
            ((Activity) imgAlbum.getContext()).runOnUiThread(() -> setImage(d));
        }).start();
    }

    private void setImage(Drawable img) {

        if (img instanceof BitmapDrawable) {
            BitmapDrawable bd = (BitmapDrawable) img;
            Bitmap bmp = bd.getBitmap();
            int dp = (int) Math.ceil(UnitConverter.convertDpToPixel(60));
            Bitmap nbmp = Bitmap.createScaledBitmap(bmp, dp, dp, false);
            bd = new BitmapDrawable(imgAlbum.getResources(), nbmp);

            imgAlbum.setImageDrawable(bd);
        } else
            imgAlbum.setImageDrawable(img);
        ((View) imgAlbum.getParent()).setVisibility(View.VISIBLE);
    }
}
