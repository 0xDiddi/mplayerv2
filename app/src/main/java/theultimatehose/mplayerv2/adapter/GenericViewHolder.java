package theultimatehose.mplayerv2.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class GenericViewHolder extends RecyclerView.ViewHolder {

    public GenericRecyclerAdapter parent;
    public int viewType;

    public GenericViewHolder(View itemView, int viewType, GenericRecyclerAdapter parent) {
        super(itemView);
        this.viewType = viewType;
        this.parent = parent;
    }

    public abstract void bind(int position, Object[] args);

}
