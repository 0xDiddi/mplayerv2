package theultimatehose.mplayerv2.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.reflect.Constructor;

public class GenericRecyclerAdapter<VH extends GenericViewHolder> extends RecyclerView.Adapter<VH> {

    private final Class<VH> holderClass;
    public Context context;

    private SparseIntArray types;    // type, layout
    private SparseArray<ChildItem> items;    // pos , type

    public GenericRecyclerAdapter(Context context, Class<VH> holderClass) {
        this.holderClass = holderClass;
        this.context = context;

        types = new SparseIntArray();
        items = new SparseArray<>();
    }

    public void addViewType(int viewType, @LayoutRes int layout) {
        types.put(viewType, layout);
    }

    public void addViewChild(int pos, int viewType, Object... bindArgs) {
        items.put(pos, new ChildItem(viewType, bindArgs));
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        try {
            View v = LayoutInflater.from(context).inflate(types.get(viewType), parent, false);
            Constructor<VH> ctor = holderClass.getConstructor(View.class, int.class, GenericRecyclerAdapter.class);
            return ctor.newInstance(v, viewType, this);
        } catch (Exception e) {
            Log.e("MPlayer v2", "Constructor for ViewHolder must be present. Report to dev (including exception).", e);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        holder.bind(position, items.get(position).bindArgs);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position).viewType;
    }

    private class ChildItem {
        int viewType;
        Object[] bindArgs;

        ChildItem(int viewType, Object[] bindArgs) {
            this.viewType = viewType;
            this.bindArgs = bindArgs;
        }

    }

}
