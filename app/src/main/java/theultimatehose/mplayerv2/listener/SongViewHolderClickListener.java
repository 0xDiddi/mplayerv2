package theultimatehose.mplayerv2.listener;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import theultimatehose.mplayerv2.R;
import theultimatehose.mplayerv2.adapter.songlist.SongViewHolder;
import theultimatehose.mplayerv2.song.Queue;

public class SongViewHolderClickListener implements View.OnClickListener {
    private Context context;
    private int songIndex;
    private SongViewHolder holder;
    private String songTitle;

    public SongViewHolderClickListener(Context context, int songIndex, SongViewHolder holder, String songTitle) {
        this.context = context;
        this.songIndex = songIndex;
        this.holder = holder;
        this.songTitle = songTitle;


    }

    @Override
    public void onClick(View v) {
        Queue.setActivePlaylist(holder.parentListReference, false);
        Queue.push(songIndex);
        Snackbar.make(holder.itemView, context.getString(R.string.snackbar_song_added, songTitle), Snackbar.LENGTH_SHORT).show();
    }
}
