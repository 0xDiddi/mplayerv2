package theultimatehose.mplayerv2.util;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class FragmentUtil {

    @SuppressLint("RestrictedApi")
    public static void removeAllFragments(FragmentManager fm) {
        FragmentTransaction ft = fm.beginTransaction();
        if (fm.getFragments() != null)
            for (Fragment f : fm.getFragments()) ft.remove(f);
        ft.commitNow();
    }

}
