package theultimatehose.mplayerv2.util;

import android.support.annotation.IdRes;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class ViewUtil {

    public static List<View> getViewsById(View root, @IdRes int id) {
        List<View> result = new ArrayList<>();
        if (root instanceof ViewGroup) {
            final int childCount = ((ViewGroup) root).getChildCount();
            for (int i = 0; i < childCount; i++)
                result.addAll(getViewsById(((ViewGroup) root).getChildAt(i), id));
        }
        if (root.getId() == id) result.add(root);
        return result;
    }

}
